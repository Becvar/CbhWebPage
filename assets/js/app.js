/**
 * Created by becva on 10.09.2016.
 */
$(document).ready(function () {
    function animateBubble(parentId, i) {
        $(parentId + ' .picture-container').css('visibility', 'visible').addClass('animated bounceIn');
        setTimeout(function () {
            $(parentId + ' .animation.' + i).css('visibility', 'visible').addClass('animated bounceIn');
            i++;
            if (i <= 10) {
                animateBubble(parentId, i);
            }
        }, 300)
    }

    function isValidToPopUp(anchorLink) {
        hash = window.location.hash.substring(1);
        return hash == anchorLink || (hash == '' && anchorLink == 'basic-info');
    }

    var sectionColors = ['#2196F3', '#3F51B5', '#9C27B0', '#F44336', '#FF5722'];
    var pages = ['basic-info', 'skills', 'projects', 'hobbies', 'contacts'];
    $('#fullpage').fullpage({
        sectionsColor: sectionColors,
        anchors: pages,
        menu: '#menu',
        css3: true,
        scrollingSpeed: 1000,
        afterLoad: function (anchorLink, index) {
            parent = 'div[data-anchor=' + anchorLink + ']';
            if (isValidToPopUp(anchorLink) && !$(parent).hasClass('visible')) {
                if ($('#menu').hasClass('active') && !wideEnough()) {
                    toggleMenu();
                }
                setMenuColors(index);
                animateBubble(parent, 1);
                $(parent).addClass('visible');
            }
        },
        onLeave: function (index, nextIndex) {
            stripMenuTriggerColor(index);
            setMenuColors(nextIndex);
        }
    });
    function wideEnough() {
        return $(window).width() >= 1550;
    }

    function stripMenuTriggerColor(index) {
        page = pages[index - 1];
        $('.menu-icon-wrapper').removeClass('waves-' + page);
    }

    function setMenuColors(index) {
        page = pages[index - 1];
        $('header').attr('data-color', page);
        $('.menu-icon-wrapper').addClass('waves-' + page);
    }

    function toggleBubbleAnimation(i) {
        $.fn.fullpage.setAllowScrolling(false);
        parent = 'div[data-anchor=' + $('header').attr('data-color') + ']';
        setTimeout(function () {
            if (i == 1) {
                $(parent + ' .animated').removeClass('bounceIn').addClass('bounceOut');
                toggleBubbleAnimation(2);
            } else {
                $(parent + ' .animated').removeClass('animated bounceOut').css('visibility', 'hidden');
                animateBubble(parent, 1);
                $.fn.fullpage.setAllowScrolling(true);
            }
        }, 650 * i);
    }

    /// MENU
    $('#fullpage, #menu-collapse, #menu-slide-in').click(function () {
        if ($(this).attr('id') == 'fullpage') {
            if ($('#menu').is(':visible')) {
                toggleMenu();
            }
        } else {
            toggleMenu();
        }
    });

    $('#toggle-animation').click(function () {
        if (!wideEnough()) {
            toggleMenu(function () {
                toggleBubbleAnimation(1);
            });
        } else {
            toggleBubbleAnimation(1);
        }
    })

    function toggleMenu(callback) {
        if (callback == undefined) {
            callback = function () {
            };
        }
        $('#menu').toggle('slide', {
            direction: 'left',
        }, 300, callback).toggleClass('active');
    }

    $('.inner-list-button').click(function () {
        $(this).find('.inner-list-arrow').toggleClass('active');
        $('#' + $(this).attr('data-toggle')).slideToggle();
    });
});

// BOOKMARK
$(function () {
    $('#bookmark-button').click(function () {
        if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
            window.sidebar.addPanel(document.title, window.location.href, '');
        } else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
            window.external.AddFavorite(location.href, document.title);
        } else if (window.opera && window.print) { // Opera Hotlist
            this.title = document.title;
            return true;
        } else { // webkit - safari/chrome
            alert('This feature is not supported by this browser. Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
        }
    });
});